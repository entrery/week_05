package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Country implements Serializable {

    /**
     * Country name.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "Name")
    private String name;


    /**
     * Artist list.
     */
    @JsonProperty()
    @JacksonXmlElementWrapper(localName = "ARTIST", useWrapping = false)
    @JacksonXmlProperty(localName = "Artist")
    private List<Artist> artists = new ArrayList<>();

    /**
     * default constructor.
     */
    public Country() {

    }

    /**
     * Constructor with arguments.
     *
     * @param name1
     */
    public Country(final String name1) {
        this.name = name1;

    }

    /**
     * get country name.
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * get country list.
     *
     * @return artists
     */
    public List<Artist> getArtists() {
        return artists;
    }

    /**
     * set country name.
     *
     * @param name1
     */
    public void setName(final String name1) {
        this.name = name1;
    }

    /**
     * set country list.
     *
     * @param artists1
     */
    public void setArtists(final List<Artist> artists1) {
        this.artists = artists1;
    }

    /**
     * to string.
     */
    @Override
    public String toString() {
        return "Country{"
                +
                "name='" + name + '\''
                +
                ", artists=" + artists
                +
                '}';
    }

    private static final long serialVersionUID = 2608L;
}
