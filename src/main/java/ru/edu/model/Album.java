package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;

public class Album implements Serializable {

    /**
     * Album name.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;

    /**
     * year of release.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "year", isAttribute = true)
    private long year;

    /**
     * constructor empty.
     */
    public Album() {
    }

    /**
     * constructor with arguments.
     *
     * @param name1
     * @param year1
     */
    public Album(final String name1, final long year1) {
        this.name = name1;
        this.year = year1;
    }

    /**
     * get name.
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * get year.
     * @return year
     */
    public long getYear() {
        return year;
    }

    /**
     * set name.
     * @param name1
     */
    public void setName(final String name1) {
        this.name = name1;
    }

    /**
     * set year.
     * @param year1
     */
    public void setYear(final long year1) {
        this.year = year1;
    }

    private static final long serialVersionUID = 2603L;

}
