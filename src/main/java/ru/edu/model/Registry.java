package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@JacksonXmlRootElement(localName = "ArtistRegistry")
public class Registry implements Serializable {


    /**
     * Country list.
     */
    @JacksonXmlElementWrapper(useWrapping = false)
    @JsonProperty("Country")
    private List<Country> countries = new ArrayList<>();

    /**
     * default constructor.
     */
    public Registry() {
    }

    /**
     * Constructor with arguments.
     *
     * @param countries1
     */
    public Registry(final List<Country> countries1) {
        this.countries = countries1;
    }

    /**
     * get country list.
     *
     * @return countries
     */
    public List<Country> getCountries() {
        return countries;
    }

    /**
     * get country.
     *
     * @param country1
     */
    public void addCountry(final Country country1) {
        countries.add(country1);
    }


    /**
     * set country list.
     *
     * @param countries1
     */
    public void setCountries(final List<Country> countries1) {

        this.countries = countries1;
    }

    /**
     * to string.
     */
    @Override
    public String toString() {
        return "Registry{"
                +
                "countries=" + countries
                +
                '}';
    }

    private static final long serialVersionUID = 2700L;
}
