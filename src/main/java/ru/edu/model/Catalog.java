package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

public class Catalog {

    /**
     * List CD.
     */
    @JacksonXmlElementWrapper(localName = "CD", useWrapping = false)
    @JacksonXmlProperty(localName = "CD")
    @JsonProperty("CD")
    private List<CD> cdList = new ArrayList<>();

    /**
     * default constructor.
     */
    public Catalog() {
    }

    /**
     * with argument.
     *
     * @param cdList1
     */
    public Catalog(final List<CD> cdList1) {
        this.cdList = cdList1;
    }

    /**
     * get List CD.
     *
     * @return cdList
     */
    public List<CD> getCdList() {
        return cdList;
    }

    /**
     * to string.
     */
    @Override
    public String toString() {
        return "Catalog{"
                +
                "cdList=" + cdList
                +
                '}';
    }
}
