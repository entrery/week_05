package ru.edu.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.edu.RegistryExternalStorage;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.File;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class RegistryExternalStorageJson implements RegistryExternalStorage {


    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     * @return
     */
    @Override
    public Object readFrom(final String filePath) {
        if (filePath == null) {
            throw new IllegalArgumentException("Wrong filePath");
        }

        ObjectMapper mapper = new ObjectMapper();

        try {
            String json = streamToString(new FileInputStream(filePath));
            Registry registry = mapper.readValue(json, Registry.class);
            return registry;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param registry реестр
     */
    @Override
    public void writeTo(final String filePath, final Registry registry) {
        if (filePath == null) {
            throw new IllegalArgumentException("Wrong filePath");
        }


        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File(filePath), registry);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String streamToString(final InputStream inputStream)
            throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;

        BufferedReader br = new BufferedReader(new
                InputStreamReader(inputStream));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }


}
