package ru.edu.model;

import ru.edu.RegistryExternalStorage;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;


public class RegistryExternalStorageJava implements RegistryExternalStorage {


    /**
     * Чтение из файла.
     *
     * @param filePath путь до файла
     * @return
     */
    @Override
    public Object readFrom(final String filePath) {
        if (filePath == null) {
            throw new IllegalArgumentException(" Wrong filepath");
        }
        Registry registry;

        try (FileInputStream input = new FileInputStream(filePath);
             ObjectInputStream objectInputStream =
                     new ObjectInputStream(input)) {
            registry = (Registry) objectInputStream.readObject();
            return registry;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Запись реестра в файл.
     *
     * @param filePath путь
     * @param registry реестр
     */
    @Override
    public void writeTo(final String filePath, final Registry registry) {

        if (filePath == null) {
            throw new IllegalArgumentException("Wrong filepath");
        }

        try (FileOutputStream input = new FileOutputStream(filePath);
             ObjectOutputStream objectInputStream =
                     new ObjectOutputStream(input)) {
            objectInputStream.writeObject(registry);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
