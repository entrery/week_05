package ru.edu.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class CD {

    /**
     * title.
     */
    @JacksonXmlProperty(localName = "TITLE")
    private String title;

    /**
     * artist.
     */
    @JacksonXmlProperty(localName = "ARTIST")
    private String artist;

    /**
     * Country.
     */
    @JacksonXmlProperty(localName = "COUNTRY")
    private String country;

    /**
     * company.
     */
    @JacksonXmlProperty(localName = "COMPANY")
    private String company;

    /**
     * price.
     */
    @JacksonXmlProperty(localName = "PRICE")
    private double price;

    /**
     * year.
     */
    @JacksonXmlProperty(localName = "YEAR")
    private int year;


    /**
     * default constructor.
     */
    public CD() {
    }

    /**
     * with arg.
     *
     * @param country1
     * @param artist1
     * @param company1
     * @param price1
     * @param title1
     * @param year1
     */
    public CD(final String title1,
              final String artist1,
              final String country1,
              final String company1,
              final double price1,
              final int year1) {
        this.title = title1;
        this.artist = artist1;
        this.country = country1;
        this.company = company1;
        this.price = price1;
        this.year = year1;
    }

    /**
     * get title.
     *
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * get artist.
     *
     * @return artist
     */
    public String getArtist() {
        return artist;
    }

    /**
     * get country.
     *
     * @return country
     */
    public String getCountry() {
        return country;
    }

    /**
     * ger company.
     *
     * @return company
     */
    public String getCompany() {
        return company;
    }

    /**
     * get price.
     *
     * @return price
     */
    public double getPrice() {
        return price;
    }

    /**
     * get year.
     *
     * @return year
     */
    public int getYear() {
        return year;
    }

    /**
     * set title.
     *
     * @param title1
     */
    public void setTitle(final String title1) {
        this.title = title1;
    }

    /**
     * set artist.
     *
     * @param artist1
     */
    public void setArtist(final String artist1) {
        this.artist = artist1;
    }

    /**
     * set country.
     *
     * @param country1
     */
    public void setCountry(final String country1) {
        this.country = country1;
    }

    /**
     * set company.
     *
     * @param company1
     */
    public void setCompany(final String company1) {
        this.company = company1;
    }

    /**
     * set price.
     *
     * @param price1
     */
    public void setPrice(final double price1) {
        this.price = price1;
    }

    /**
     * set year.
     *
     * @param year1
     */
    public void setYear(final int year1) {
        this.year = year1;
    }

    /**
     * to string.
     */
    @Override
    public String toString() {
        return "CD{"
                +
                "title='" + title + '\''
                +
                ", artist='" + artist + '\''
                +
                ", country='" + country + '\''
                +
                ", company='" + company + '\''
                +
                ", price=" + price
                +
                ", year=" + year
                +
                '}';
    }
}
