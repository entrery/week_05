package ru.edu.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Artist implements Serializable {


    /**
     * Artist name.
     */
    @JsonProperty()
    @JacksonXmlProperty(localName = "name", isAttribute = true)
    private String name;


    /**
     * Album list.
     */
    @JacksonXmlElementWrapper(localName = "Albums")
    @JacksonXmlProperty(localName = "Album")
    private List<Album> albums = new ArrayList<>();


    /**
     * default constructor.
     */
    public Artist() {
    }

    /**
     * Constructor with arguments.
     *
     * @param name1
     */
    public Artist(final String name1) {
        this.name = name1;

    }


    /**
     * get artist name.
     *
     * @return name
     */
    @JacksonXmlProperty(localName = "Name")
    public String getName() {
        return name;
    }

    /**
     * get albums list.
     *
     * @return albums
     */
    public List<Album> getAlbums() {
        return albums;
    }


    /**
     * set albums list.
     *
     * @param albums1
     */
    public void setAlbums(final List<Album> albums1) {
        this.albums = albums1;
    }

    /**
     * to string.
     *
     */
    @Override
    public String toString() {
        return "Artist{"
                +
                "name='" + name + '\''
                +
                ", albums=" + albums
                +
                '}';
    }

    private static final long serialVersionUID = 2605L;

}
