package ru.edu.model;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class RegistryExternalStorageJavaTest {

    RegistryExternalStorageJava java = new RegistryExternalStorageJava();
    RegistryExternalStorageXml xml = new RegistryExternalStorageXml();


    static final String OUTPUT = "output/artist_by_country.serialized";
    static final String INPUT = "input/cd_catalog.xml";


    Registry registry;


    @Test
    public void readFrom() {
        Registry registry = (Registry) java.readFrom(OUTPUT);
        assertNotNull(registry);
    }
    @Test(expected = IllegalArgumentException.class)
    public void readNull() {
        java.readFrom(null);
    }

    @Test
    public void writeTo() {
        registry = (Registry) xml.readFrom(INPUT);
        java.writeTo(OUTPUT, registry);

    }

    @Test(expected = IllegalArgumentException.class)
    public void writeNull(){java.writeTo(null, registry);}

}