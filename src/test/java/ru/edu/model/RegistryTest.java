package ru.edu.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class RegistryTest {
    Country country1;
    Country country2;
    Registry registry = new Registry();
    List<Country> countries = new ArrayList<>();

    @Test
    public void getCountries() {
        registry.addCountry(country1);
        registry.addCountry(country2);
        assertEquals(2,registry.getCountries().size());
    }

    @Test
    public void setCountries() {
        countries.add(country1);
        registry.setCountries(countries);
        assertEquals(1,registry.getCountries().size());
    }

    @Test
    public void addCountry() {
        registry.addCountry(country1);
        assertEquals(1,registry.getCountries().size());
    }

    @Test
    public void testToString() {
        assertNotNull(registry.toString());
    }
}