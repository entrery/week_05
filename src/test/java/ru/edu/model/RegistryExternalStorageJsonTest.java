package ru.edu.model;

import org.junit.Test;

import java.io.FileInputStream;

import static org.junit.Assert.*;

public class RegistryExternalStorageJsonTest {

    RegistryExternalStorageJson json = new RegistryExternalStorageJson();
    RegistryExternalStorageXml xml = new RegistryExternalStorageXml();


    String INPUT = "input/cd_catalog.xml";
    String OUTPUT = "output/artist_by_country.json";

    Registry registry = new Registry();

    @Test
    public void readFrom() {
        Registry registry = (Registry) xml.readFrom(INPUT);
        json.writeTo(OUTPUT, registry);
    }

    @Test(expected = IllegalArgumentException.class)
    public void readNull(){json.readFrom(null);

    }

    @Test
    public void writeTo() {
        Registry registry = (Registry) xml.readFrom(INPUT);
        json.writeTo(OUTPUT, registry);
    }

    @Test(expected = IllegalArgumentException.class)
    public void writeNull(){json.writeTo(null, registry);}


}