package ru.edu.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ArtistTest {
    List<Album> albums = new ArrayList<>();
    Album album = new Album();
    Album album2 = new Album();
    Artist artist = new Artist("Джонстон");

    @Test
    public void getName() {
        assertEquals("Джонстон", artist.getName());
    }

    @Test
    public void getAlbums() {
        albums.add(album);
        assertNotNull(artist.getAlbums());
    }



    @Test
    public void setAlbums() {
        albums.add(album2);
        albums.add(album);
        artist.setAlbums(albums);
        assertEquals(2,artist.getAlbums().size());

    }
    @Test
    public void teatToString() {
        assertNotNull(artist.toString());
    }
}