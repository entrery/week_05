package ru.edu.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;

public class CatalogTest {
    List<CD> list = new ArrayList<>();
    Catalog catalog = new Catalog(list);


    @Test
    public void getCdList() {
        assertNotNull(catalog);

    }

    @Test
    public void testToString(){
        assertNotNull(catalog.toString());
    }
}