package ru.edu.model;

import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.*;

public class RegistryExternalStorageXmlTest {

    RegistryExternalStorageXml xml = new RegistryExternalStorageXml();

    String INPUT = "input/cd_catalog.xml";
    String OUTPUT = "output/artist_by_country.xml";

    Registry registry = new Registry();

    @Test
    public void readFrom() {
        Registry registry = (Registry) xml.readFrom(INPUT);

    }
    @Test(expected = IllegalArgumentException.class)
    public void readNull(){xml.readFrom(null);

    }


    @Test
    public void writeTo() {
        Registry registry = (Registry) xml.readFrom(INPUT);
        xml.writeTo(OUTPUT, registry);

    }
    @Test(expected = IllegalArgumentException.class)
    public void writeNull(){xml.writeTo(null, registry);}

}