package ru.edu.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class AlbumTest {

    Album album = new Album("БорисТ Джностон", 1980);


    @Test
    public void getName() {
        assertEquals("БорисТ Джностон",album.getName());
    }

    @Test
    public void getYear() {
        assertEquals(1980,album.getYear());
    }

    @Test
    public void setName() {
        album.setName("NoOne");
        assertEquals("NoOne",album.getName());
    }

    @Test
    public void setYear() {
        album.setYear(1990);
        assertEquals(1990,album.getYear());
    }
}