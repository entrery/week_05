package ru.edu.model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class CountryTest {

    List<Artist> artists = new ArrayList<>();
    Artist artist1 = new Artist();
    Country country = new Country("England");

    @Test
    public void getName() {
        assertEquals("England", country.getName());
    }

    @Test
    public void getArtists() {
        assertNotNull(country.getArtists());
    }

    @Test
    public void setName() {
        country.setName("USA");
        assertEquals("USA", country.getName());
    }

    @Test
    public void setArtists() {
        country.setArtists(artists);
        assertNotNull(country.getArtists());
    }

    @Test
    public void testToString() {
        assertNotNull(country.toString());
    }
}