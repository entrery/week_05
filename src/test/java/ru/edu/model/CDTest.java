package ru.edu.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class CDTest {
    CD cd = new CD("Alabama",
            "Boris",
            "Eng",
            "Alfa",
            109.9,
            1980);

    @Test
    public void getTitle() {
        assertEquals("Alabama", cd.getTitle());
    }

    @Test
    public void getArtist() {
        assertEquals("Boris", cd.getArtist());
    }

    @Test
    public void getCountry() {
        assertEquals("Eng", cd.getCountry());
    }

    @Test
    public void getCompany() {
        assertEquals("Alfa", cd.getCompany());
    }

    @Test
    public void getPrice() {
        assertEquals(109.9, cd.getPrice(),1);
    }

    @Test
    public void getYear() {
        assertEquals(1980, cd.getYear());
    }

    @Test
    public void setTitle() {
        cd.setTitle("1");
        assertEquals("1", cd.getTitle());
    }

    @Test
    public void setArtist() {
        cd.setArtist("1");
        assertEquals("1", cd.getArtist());
    }

    @Test
    public void setCountry() {
        cd.setCountry("1");
        assertEquals("1", cd.getCountry());
    }

    @Test
    public void setCompany() {
        cd.setCompany("1");
        assertEquals("1", cd.getCompany());
    }

    @Test
    public void setPrice() {
        cd.setPrice(120.1);
        assertEquals(120.1, cd.getPrice(),1);
    }

    @Test
    public void setYear() {
        cd.setYear(2000);
        assertEquals(2000, cd.getYear());
    }

    @Test
    public void testToString() {
        assertNotNull(cd.toString());
    }
}